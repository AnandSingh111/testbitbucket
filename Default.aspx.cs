﻿using System;
using System.IO;
using System.Web.UI;

namespace TestBitBucket
{
    public partial class _Default : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            StreamReader sr = File.OpenText(Server.MapPath("1.txt"));
            string strContents = sr.ReadToEnd();
            //To display normal raw contents
            Response.Write(strContents);

            //To handle Carriage returns
            Response.Write(strContents.Replace("\n", "<br>"));

            sr.Close();

            Main();
            Main1();
            Main2();
        }

        public static void Main()
        {
            int num = 0, reverseNumber = 0, temp, rem;

            Console.Write("Enter a Number : ");
            //read number and parse it for int
            num = int.Parse(Console.ReadLine());

            temp = num;
            //loop through number
            while (temp != 0)
            {
                rem = temp % 10; //get remainder, means last digit of number
                temp = temp / 10; // get remaining value
                reverseNumber = reverseNumber * 10 + rem;   //save it is new number       
            }
            //if number = reverse number
            if (num == reverseNumber)
            {
                Console.WriteLine(num + " is a palindrome number");
            }
            else
            {
                Console.WriteLine(num + " is not a palindrome number");
            }
        }

        public static void Main1()
        {
            //declare two numbers and temp variable
            int num1, num2, temp;
            //ask for numbers
            Console.WriteLine("\nEnter the First Number : ");
            num1 = int.Parse(Console.ReadLine());
            Console.WriteLine("\nEnter the Second Number : ");
            num2 = int.Parse(Console.ReadLine());
            //swap them using third variable
            temp = num1;
            num1 = num2;
            num2 = temp;
            //print numbers
            Console.WriteLine("After Swapping : ");
            Console.WriteLine("First Number : " + num1);
            Console.WriteLine("Second Number : " + num2);

        }

        public static void Main2()
        {
            string[] array1 = { "cat", "dogs", "donkey", "camel" };
            string ItemToSearch = "dogs";
            bool ItemFound = false;

            for (int i = 0; i < array1.Length; i++)
            {
                if (array1[i] == ItemToSearch)
                {
                    ItemFound = true;
                }
            }

            if (ItemFound)
            {
                Console.WriteLine(ItemToSearch + " found");
            }
            else
            {
                Console.WriteLine(ItemToSearch + " is not found");
            }
        }
    }     
}